defmodule MlMaze do
  @moduledoc """
  Documentation for `MlMaze`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> MlMaze.hello()
      :world

  """
  def hello do
    :world
  end
end
